#Alpine provides Lua 5.3 as Lua5.3, Lua is by default symlinked to Lua 5.1
#This can be changed to simply lua as needed/desired
LUA ?= lua5.3
LUAV = 5.3
LUA_SHARE=/usr/share/lua/$(LUAV)
#On Debian /usr/lib/x86_64-linux-gnu/liblua5.3.a would be used, Alpine packages it a little differently, so we use the following
STATIC_LUA_LIB ?= /usr/lib/liblua-5.3.so.0.0.0
LUA_INCLUDE_DIR ?= /usr/include/lua5.3
LUA_ROCKS ?= /usr/bin/luarocks-5.3
DESTDIR=/usr/bin

fetch-libraries:
	$(LUA_ROCKS) install lume --local
	$(LUA_ROCKS) install luacheck --local
	$(LUA_ROCKS) install lsqlite3 --local

compile-lua:
	fennel --compile --require-as-include src/tkts.fnl > src/tkts.lua
	sed -i '1 i\-- Author: Will Sinatra <wpsinatra@gmail.com> | License: GPLv3' src/tkts.lua
	sed -i '1 i\#!/usr/bin/$(LUA)' src/tkts.lua

install-lua:
	install ./src/tkts.lua -D $(DESTDIR)/tkts

#Ask Techno about this one
compile-bin:
	CC_OPTS="-L/usr/lib/lua/5.3/ -llsqlite3 -static" cd ./src/ && fennel --compile-binary tkts.fnl tkts-bin $(STATIC_LUA_LIB) $(LUA_INCLUDE_DIR) --native-module /usr/lib/lua/5.3/lsqlite3.so

install-bin:
	install ./src/tkts-bin -D $(DESTDIR)/tkts

check:
	luacheck ./src/tkts.lua
