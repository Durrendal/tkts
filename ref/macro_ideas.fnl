;;This doesn't precisely work.. it fails to pass the value out of the macro
(macro tkts/select [db sql func tbl]
  "Create a select with return function"
  `(let
       [database# ,db
        function# (fn [funcname#] (each [row# (database#:nrows ,sql)] (funcname# row#)))
        count# (fn [tbl#] (. tbl# "count(id)"))]
     (if (= ,func "count")
         (function# count#))))

(fn dev/count []
  (let
      [db (sql.open conftbl.tkt_loc)
       sql "SELECT count(id) FROM tickets "
       ret {}
       count {"total" 0 "open" 0 "closed" 0}]
    (macrodebug (tkts/select db (.. sql ";") "count") ret)
    (print (inspect ret))
    ;;(tkts/select db (.. sql "WHERE status = Open") (tset count "open" (. row# "count(id)")))
    ;;(tkts/select db (.. sql "WHERE status = Closed") (tset count "closed" (. row# "count(id)")))
    count))
