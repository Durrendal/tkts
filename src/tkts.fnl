#!/usr/bin/fennel
;;===== Deps =====
(local lume (require :lume))
(local sql (require :lsqlite3))
(local inspect (require :inspect))

;;===== Global Vars =====
(global ver {"db" 1
             "tkts" 2.0})

(global dot {"dir" (.. (os.getenv "HOME") "/.config/tkts/")
             "file" (.. (os.getenv "HOME") "/.config/tkts/tkts.conf")})

(var conf {"editor" (os.getenv "EDITOR")
           "mail" "mutt -s"
           "rate" "0.00"
           "user" (os.getenv "USER")
           "company" "Change Me"
           "tkt_loc" (.. dot.dir "tkts.db")
           "invoice_template" (.. dot.dir "invoice.tf")})

;;===== Invoice Template =====
(global invoice_template ".lj
.ps +3
.UL \"{full_name}\"
\\*<
.br
{address}
.br
{cell_number}
\\*>

.lj
.TS
tab(+) expand;
lUP14 s s s .
Bill To+++
_
.T&
l s lB r .
{company}+Invoice No:+{invoice_number}
{company_addr}+Invoice Date:+{issued_date}
{company_zip}+Due Date:+{due_date}
{client_email}++
{client_cellphone}++
.TE

.TS
tab(@) expand box;
cB | cB | cB | cB
l | n | n | n .
Description@QTY/HR@Unit Price@Total
_
{tkt_title}@{hours}@${hourly}@${hours_cost}
_
.T&
c c | rB r .
@@Subtotal@${hours_cost}
@@Tax Rate@0.0%
@@Total Tax@$0.00
@@Balance Due@${hours_cost}
.TE
")

;;===== DB Schema =====
(global schematbl {"version"
                   "CREATE TABLE IF NOT EXISTS version (
db_ver INTEGER NOT NULL);"
                   "tickets"
                   "CREATE TABLE IF NOT EXISTS tickets (
id INTEGER PRIMARY KEY,
status TEXT CHECK(status IN ('Open', 'Closed')) DEFAULT 'Open' NOT NULL,
title TEXT NOT NULL,
desc TEXT,
client TEXT,
project TEXT,
owner TEXT NOT NULL,
time INTEGER DEFAULT 0 NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP);"
                   "records"
                   "CREATE TABLE IF NOT EXISTS records (
id INTEGER PRIMARY KEY,
tkt_num INTEGER NOT NULL,
log TEXT NOT NULL,
user TEXT NOT NULL,
time INTEGER DEFAULT 0 NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY (tkt_num) REFERENCES tickets (id) ON DELETE CASCADE);"
                   "tasks"
                   "CREATE TABLE IF NOT EXISTS tasks (
id INTEGER PRIMARY KEY,
tkt_num INTEGER NOT NULL,
title TEST NOT NULL,
state INTEGER DEFAULT 0 NOT NULL,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY (tkt_num) REFERENCES tickets (id) ON DELETE CASCADE)
CHECK (state >= 0 AND state <= 2);
" ;; [] = 0 [-] = 1 [x] = 2
                   "company"
                   "CREATE TABLE IF NOT EXISTS company (
id INTEGER PRIMARY KEY,
name TEXT UNIQUE NOT NULL,
abbrv TEXT NOT NULL,
address TEXT,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP);"
                   "client"
                   "CREATE TABLE IF NOT EXISTS client (
id INTEGER PRIMARY KEY,
name TEXT NOT NULL,
company TEXT NOT NULL,
role TEXT,
phone TEXT,
email TEXT,
created_on DATETIME DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY (company) REFERENCES company (abbrv) ON DELETE CASCADE);"})
;;Additional args/functions to open an email client, and return a contact card.
;;Then more filters for status/report to sort by client
;;Potentially filter existing tickets by project and associate subtickets with project tickets

(fn db/defnschema []
  "Create database schema"
  (let [db (sql.open conf.tkt_loc)]
    (db:exec schematbl.version)
    (db:exec (.. "INSERT INTO version (db_ver) VALUES (" ver.db ");"))
    (db:exec schematbl.tickets)
    (db:exec schematbl.records)
    ;;(db:exec schematbl.tasks)
    (db:exec schematbl.company)
    (db:exec schematbl.client)
    (db:close)))

;;Not what we're doing long term, but will be OK for testing.
(fn db/migrate []
  "Migrate between schema versions (really just move the old db..)"
  (let
      [db (sql.open conf.tkt_loc)]
    ;;if db_ver != ver.db defined in schema, migrate
    (each [row (db:nrows "SELECT db_ver from version;")]
      (if (~= (. row "db_ver") ver.db)
          (os.execute (.. "mv " conf.tkt_loc " " dot.dir "tkts_" (. row "db_ver") ".db"))))))

;;===== util =====
(fn util/split [val check]
  "Split string on delim and return as table"
    (if (= check nil)
        (do
         (local check "%s")))
    (local t {})
    (each [str (string.gmatch val (.. "([^" check "]+)"))]
          (do
           (table.insert t str)))
    t)

(fn util/exists? [f]
  "Check if file exists"
  (local f? (io.open f "r"))
  (if f?
      (f?:close)
      false))

(fn util/dir_exists? [dir]
  (let
      [(stat err code) (os.rename dir dir)]
    (when (= stat nil)
      (when (= err 13)
        (lua "return true"))
      (lua "return false"))
    true))

(fn util/mitigate []
  "Ensure dotfile consistency between versions"
  (local valid_vars ["editor" "mail" "rate" "user" "tkt_loc" "invoice_template" "company"])
  (local valid_holds [(os.getenv "EDITOR") "mutt -s" "0.00" (os.getenv "USER") (.. dot.dir "tkts.db") (.. dot.dir "invoice.tf") "Change Me"])
  (var reset nil)
  ;;If there is a value for each setting, leave it be
  (each [k v (pairs valid_vars)]
    (do
      (if (= (. conf v) nil)
          (do
            ;;set holds to match config values
            (tset conf v (. valid_holds k))
            (set reset true)))))
  ;;If a value is missing re-serialize the file
  (if (= reset true)
      (with-open [f (io.open dot.file "w")]
        (f:write (inspect conf)))))

(fn util/conf []
  "Load dotfile configuration"
  ;;if .config/tkts exists
  (if (util/dir_exists? dot.dir)
      (do
        ;;if .config/tkts/tkts.conf exists
        (if (util/exists? dot.file)
            (do
              ;;load config & verify all settings exist
              (with-open [f (io.open dot.file "r")]
                (set conf (lume.deserialize (f:read "*a"))))
              (util/mitigate))
            ;;else, create a new one with defaults
            (with-open [f (io.open dot.file "w")]
              (f:write (inspect conf))))
        ;;if tkts.db exists, check if migration is needed or not, if it doesn't exist create it
        (if (util/exists? conf.tkt_loc)
            (db/migrate)
            (db/defnschema))
        (if (util/exists? conf.invoice_template)
            nil
            (with-open [f (io.open conf.invoice_template "w")]
              (f:write invoice_template))))
      (do
        ;;if ~/.config/tkts doesn't exist create it, serialize a new tkts.conf & create a db, and populate groff templates
        (os.execute (.. "mkdir " dot.dir))
        (with-open [f (io.open dot.file "w")]
          (f:write (inspect conf)))
        (with-open [f (io.open conf.invoice_template "w")]
          (f:write invoice_template))
        (db/defnschema))))

;;(util/f->v "--tkt") -> tkt
;;(util/f->v "-t") -> tkt
(fn util/f->v [flag]
  "Convert long and short flags to keywords."
  (var val "")
  ;;This feels limiting, perhaps there's a better way to do it?
  (let
      [flagtbl {"t" "tkt"
                "n" "name"
                "l" "log"
                "h" "help"
                "i" "issue"
                "f" "filter"
                "r" "records"
                "c" "company"
                "v" "verbose"
                "s" "segment"
                "addr" "address"
                "d" "description"
                "abv" "abbreviation"}]
    (if
     (string.find flag "%-%-")
     (set val (string.gsub flag "%-%-" ""))
     (string.find flag "%-%a")
     (set val (. flagtbl (string.gsub flag "%-" ""))))
    val))

;;(util/farse ["--help" "-h] ?args)
(fn util/farse [flags args]
  "Parse a table of arguments against a table of valid flags, and convert to a keyed table"
  (local assoc {})
  (each [k v (pairs args)]
    (for [i 1 (# flags)]
      (do
        (var curkey "")
        (if (= v (. flags i))
            (do
              (set curkey (util/f->v (. flags i)))
              (if (= curkey "verbose")
                  (tset assoc curkey true)
                  (tset assoc curkey (. args (+ k 1)))))))))
  assoc)

;;(util/err "tkt" args "A tkt number is required to do X with Y.")
(fn util/err [arg args msg]
  (if (= (. args arg) nil)
      (do
        (io.stderr:write (.. msg "\n"))
        (os.exit))))

;;(util/template {1 "Templates are just strings with {variables}" :variables "Something substituted"})
(fn util/template [str vars]
  (when (not vars)
    (let
        [vars str
         str (. vars 1)]
      (string.gsub str "({([^}]+)})"
                   (fn [whole i]
                     (or (. vars i) whole))))))

;;===== sql =====
;;(sql/modify (sql.open conf.tkt_loc) 1 "tickets" ["time" "status"] {"time" 123 "status" "Closed"})
(fn sql/modify [db id table coltbl valtbl]
  "Update existing columns with specified values"
  (if (or (~= (type coltbl) "table") (~= (type valtbl) "table"))
      (do
        (io.stderr:write "Invalid table passed to sql/modify\n")
        (os.exit)))
  (var update "")
  ;;collate coltbl values with valtbl values using coltbl as keys
  (each [k v (ipairs coltbl)]
    (set update (.. update (.. "'" v "' = '" (-> (. valtbl v)
                                                 (string.gsub "'" "''")) "',"))))
  ;;:sub 1 -2 here removes the last trailing ,
  (let
      [stmt (db:prepare (.. "UPDATE " table " SET " (update:sub 1 -2) " WHERE id = " id ";"))]
    (stmt:step)
    (stmt:finalize)))

;;(sql/select (sql.open conf.tkt_loc) "client" ["email" "role"] "LIKE" ["name"] ["will"]) <- select where name like will
;;(sql/select (sql.open conf.tkt_loc) "client" ["email" "role"] "LIKE" ["name"] [""]) <- select anything
;;(sql/select (sql.open conf.tkt_loc) "tickets" ["client" "title"] "=" ["id"] [10]) <- select where X = Y
;;(sql/select (sql.open conf.tkt_loc) "company" ["name"] "!=" ["abbrv"] ["LC"]) <- select where X not like Y
(fn sql/select [db sqltbl coltbl fltrtyp fltrtbl valtbl]
  (if (or (~= (type coltbl) "table") (~= (type fltrtbl) "table") (~= (type valtbl) "table"))
      (do
        (io.stderr:write "Invalid table passed to sql/select.\n")
        (os.exit))
      (and (~= fltrtyp "LIKE") (~= fltrtyp "=") (~= fltrtyp "!="))
      (do
        (io.stderr:write (.. fltrtyp " is an invalid filter type. Try LIKE, =, or !=.\n"))
        (os.exit)))
  (var selection "")
  (var filter "")
  (each [k v (ipairs coltbl)]
    (set selection (.. selection (.. "\"" v "\","))))
  (if (> (length fltrtbl) 1)
      (each [k v (ipairs fltrtbl)]
        (set filter (.. filter (.. "\"" (. valtbl k) "\","))))
      (set filter (.. filter (.. (. fltrtbl 1) " " fltrtyp " '" (if (= fltrtyp "LIKE") "%" "") (. valtbl 1) (if (= fltrtyp "LIKE") "%" "") "'"))))
  ;;:sub 1 -2 here removes the last trailing ,
  ;;(print (.. "SELECT " (selection:sub 1 -2) " FROM " sqltbl " WHERE " filter ";"))
  (let
      [stmt (db:prepare (.. "SELECT " (selection:sub 1 -2) " FROM " sqltbl " WHERE " filter ";"))
       retval []]
    (each [row (stmt:nrows)]
      (table.insert retval row))
    (stmt:finalize)
    retval))

;;===== company =====
;;(comp/add {"name" "Lambdacreate" "abbreviation" "LC" "address" "123 Nihil et Nowhere"})
(fn comp/add [args]
  "Add a company"
  (util/err "name" args "You must provide a name to add a company")
  (util/err "abbreviation" args "You must provide an abbreviation to add a company")
  (let
      [db (sql.open conf.tkt_loc)
       stmt (db:prepare "INSERT into company (name,abbrv,address) VALUES (:name, :abbrv, :address)")]
    (stmt:bind_names {:name args.name :abbrv args.abbreviation :address args.address})
    (stmt:step)
    (stmt:finalize))
  (print (.. "{:Company " args.name "}")))

;;(comp/edit {"name" "LambdaCreate"})
(fn comp/edit [args]
  "Edit a company"
  (util/err "name" args "A company name is required to edit a company record.")
  (let
      [db (sql.open conf.tkt_loc)]
    (each [row (db:nrows (.. "SELECT * from company WHERE name = '" args.name "';"))]
      (let
          [company {"name" (. row "name")
                    "abbrv" (. row "abbrv")
                    "address" (if (= (. row "address") nil) "" (. row "address"))}
           id (. row "id")
           tmpf (os.tmpname)]
        (var diff {})
        (with-open [f (io.open tmpf "w")]
          (f:write (inspect company)))
        (os.execute (.. conf.editor " " tmpf))
        (with-open [f (io.open tmpf "r")]
          (set diff (lume.deserialize (f:read "*a"))))
        (sql/modify db id "company" ["name" "abbrv" "address"] diff)))
    (db:close)))

;;(comp/list)
(fn comp/list []
  "Briefly list all companies"
  (let
      [db (sql.open conf.tkt_loc)]
    (each [row (db:nrows "SELECT * FROM company;")]
      (print (.. "{:Id " (. row "id") " :Name " (. row "name") "}")))
    (db:close)))

;;(comp/info "LambdaCreate")
(fn comp/info [name]
  "Return company as lua table"
  (var export {})
  (let
      [db (sql.open conf.tkt_loc)]
    (each [row (db:nrows (.. "SELECT * from company where name = '" name "';"))]
      (let
          [company {"id" (. row "id")
                    "name" (. row "name")
                    "abbrv" (. row "abbrv")
                    "address" (. row "address")
                    "created_on" (. row "created_on")}]
        (set export company))))
  export)

;;===== client =====
;;TODO: Redo this function so it relies on editing a lua table via a tmpf
;;(clie/add "Will" "LambdaCreate" "{role='Dev',phone='123-456-7890',email='wpsinatra@gmail.com'}")
(fn clie/add [args detail]
  "Add a client"
  (util/err "name" args "You must provide a name to add a client.")
  (util/err "company" args "You must provide a company to associate the client with.")
        ;;figure out a way to catch this error, so that you can io.stderr instead of getting an <eof> if you get a malformed table
  (let
      [details (if (~= detail nil) (lume.deserialize detail) "")]
    (if (and (~= detail nil) (~= (type details) "table"))
        (do
          (io.stderr:write "Details must be provided as a table.\nIE: \"{role='X', phone='123-456-7890', email='nihil@nothing'}\n")
          (os.exit))
        (do
          (let
              [db (sql.open conf.tkt_loc)
               stmt (db:prepare "INSERT into client (name,company,role,phone,email) VALUES (:name, :company, :role, :phone, :email)")]
            (stmt:bind_names {:name args.name :company args.company :role details.role :phone details.phone :email details.email})
            (stmt:step)
            (stmt:finalize))
          (print (.. "{:Client " args.name "}"))))))

;;(clie/edit {"name" "Will Sinatra" "company" "LambdaCreate"})
(fn clie/edit [args]
  "Edit a client"
  (util/err "name" args "A name is required to edit a client record.")
  (util/err "company" args "The company associated with the user must be provided.")
  (let
      [db (sql.open conf.tkt_loc)]
    (each [row (db:nrows (.. "SELECT * FROM client WHERE name = '" args.name "' AND company = '" args.company "';"))]
      (let
          [client {"name" (. row "name")
                   "company" (. row "company")
                   "role" (if (= (. row "role") nil) "" (. row "role"))
                   "phone" (if (= (. row "phone") nil) "" (. row "phone"))
                   "email" (if (= (. row "email") nil) "" (. row "email"))}
           id (. row "id")
           tmpf (os.tmpname)]
        (var diff {})
        (with-open [f (io.open tmpf "w")]
          (f:write (inspect client)))
        (os.execute (.. conf.editor " " tmpf))
        (with-open [f (io.open tmpf "r")]
          (set diff (lume.deserialize (f:read "*a"))))
        (sql/modify db id "client" ["name" "company" "role" "phone" "email"] diff)))
    (db:close)))

;;(clie/list)
(fn clie/list []
  "Briefly list all clients"
  (let
      [db (sql.open conf.tkt_loc)]
    (each [row (db:nrows "SELECT * FROM client;")]
      (print (.. "{:Id " (. row "id") " :Name " (. row "name") " :Comp " (. row "company") "}")))
    (db:close)))

;;(clie/info "Will Sinatra" "LambdaCreate")
(fn clie/info [name company]
  "Return company as lua table"
  (var export {})
  (let
      [db (sql.open conf.tkt_loc)]
    (each [row (db:nrows (.. "SELECT * from client where name = '" name "' AND company LIKE '" company "';"))]
      (let
          [client {"id" (. row "id")
                   "name" (. row "name")
                   "company" (. row "company")
                   "role" (if (= (. row "role") nil) "" (. row "role"))
                   "phone" (if (= (. row "phone") nil) "" (. row "phone"))
                   "email" (if (= (. row "email") nil) "" (. row "email"))
                   "created_on" (. row "created_on")}]
        (set export client))))
  export)

;;===== rec =====
(fn rec/info [tnum amount]
  "Return a table of records"
  (let
      [db (sql.open conf.tkt_loc)
       record {}
       statement (if (= amount nil)
                     (.. "SELECT * from records where tkt_num = " tnum " ORDER BY id ASC;")
                     (.. "SELECT * from records where tkt_num = " tnum " ORDER BY id DESC limit " amount ";"))]
    (each [row (db:nrows statement)]
      (let
          [rec {"id" (. row "id")
                "log" (. row "log")
                "user" (. row "user")
                "time" (. row "time")
                "created_on" (. row "created_on")}]
        (table.insert record rec)))
    record))

;;===== tkts =====
;;(tkts/count (sql.open conf.tkt_loc))
;;(tkts/count db "Total") ;;or Closed or Open
(fn tkts/count [db ?filter]
  "Return a count of tickets"
  (let
      [sql "SELECT count(id) FROM tickets "
       open "WHERE status = 'Open';"
       closed "WHERE status = 'Closed';"
       count {"open" 0 "closed" 0 "total" 0}
       query (fn [sql targ] (each [row (db:nrows sql)] (tset count targ (. row "count(id)"))))]
       (if (or (= ?filter nil) (= ?filter "Total"))
           (do
             (query (.. sql open) "open")
             (query (.. sql closed) "closed")
             (tset count "total" (+ count.open count.closed)))
           (= ?filter "Closed")
           (query (.. sql closed) "closed")
           (= ?filter "Open")
           (query (.. sql open) "open"))
       count))

;;(tkts/create {"issue" "Some problem" "description" "Some detils"})
(fn tkts/create [args]
  "Create a new open ticket"
  (util/err "issue" args "An issue title is required to create a tkt.")
  (let
      [db (sql.open conf.tkt_loc)
       status "Open"
       time 0
       stmt (db:prepare "INSERT into tickets (status,title,desc,owner,time) VALUES (:status, :title, :desc, :owner, :time)")]
    (stmt:bind_names {:status status :title args.issue :desc args.description :owner conf.user :time time})
    (stmt:step)
    (stmt:finalize)
    (let
        [tid (db:last_insert_rowid)]
      (db:close)
      (print (.. "T" tid " {:Status Open}")))))

;;(tkts/close {"tkt" 1})
(fn tkts/close [args]
  "Move a ticket from Open to Closed"
  (util/err "tkt" args "A tkt number is required to close a tkt.")
  (let
      [db (sql.open conf.tkt_loc)]
    (sql/modify db args.tkt "tickets" ["status"] {"status" "Closed"})
    (db:close))
  (print (.. "T" args.tkt " {:Status Closed}")))

;;(tkts/reopen {"tkt" 1})
(fn tkts/reopen [args]
  "Move a ticket from Closed to Open"
  (util/err "tkt" args "A tkt number is required to reopen a tkt.")
  (let
      [db (sql.open conf.tkt_loc)]
    (sql/modify db args.tkt "tickets" ["status"] {"status" "Open"})
    (db:close))
  (print (.. "T" args.tkt " {:Status Open}")))

;;(tkts/info {"tkt" 1})
;;Add a way to verify an integer is passed as tnum
(fn tkts/info [tnum]
  "Return tkt as lua table"
  (var export {})
  (let
      [db (sql.open conf.tkt_loc)]
    (each [row (db:nrows (.. "SELECT * from tickets where id = " tnum ";"))]
      (let
          [tkt {"status" (. row "status")
                "title" (. row "title")
                "desc" (if (= (. row "desc") nil) "" (. row "desc"))
                "client" (if (= (. row "client") nil) "" (. row "client"))
                "project" (if (= (. row "project") nil) "" (. row "project"))
                "owner" (. row "owner")
                "time" (. row "time")
                "rate" 0
                "log" 0}]
        (tset tkt "rate" (math.ceil (+ (* (/ tkt.time 60) (. conf "rate")) 0.5)))
        (each [row (db:nrows (.. "SELECT count(id) from records where tkt_num = " tnum ";"))]
          (tset tkt "log" (. row "count(id)")))
        (set export tkt))))
  export)

;;(tkts/comment {"tkt" 1 "log" "commentary"})
(fn tkts/comment [args]
  "Add a comment to the ticket log via editor, or if passed a string, directly into the ticket log"
  (util/err "tkt" args "A tkt number is required to add logging to a tkt.")
  (let
      [db (sql.open conf.tkt_loc)
       stmt (db:prepare "INSERT into records (tkt_num,log,user) VALUES (:tkt_num, :log, :user)")]
    (if (= args.log nil)
        (do
          (let
              [tmpf (os.tmpname)]
            (os.execute (.. conf.editor " " tmpf))
            (with-open [f (io.open tmpf "r")]
              (stmt:bind_names {:tkt_num args.tkt :log (f:read "*a") :user conf.user})
              (stmt:step)
              (stmt:finalize)))
          (let
              [rid (db:last_insert_rowid)]
            rid))
        (do
          (let
              [info (tkts/info args.tkt)]
            (stmt:bind_names {:tkt_num args.tkt :log args.log :user conf.user})
            (stmt:step)
            (stmt:finalize)
            (print (.. "T" args.tkt " {:log "  (+ info.log 1) "}")))))))

;;Add an add record as well, so that per log timing can be added manually.
;;(tkts/time {"tkt" 1})
(fn tkts/time [args]
  "Grab a time stamp, open the ticket's log, append to it, and then append the calculated time spent appending to the ticket log, to the ticket's total time in minutes."
  (util/err "tkt" args "A tkt number is required to record time on a tkt.")
  (let
      [db (sql.open conf.tkt_loc)
       time {"start" (os.time)
             "end" 0
             "diff" 0
             "cur" 0}]
    (each [row (db:nrows (.. "SELECT time from tickets WHERE id = " args.tkt ";"))] (tset time "cur" (. row "time")))
    (let
        [rnum (tkts/comment args)
         info (tkts/info args.tkt)
         tnum (if (= info.log 0) 1 info.log)]
      (tset time "end" (os.time))
      (tset time "diff" (math.ceil (+ (/ (os.difftime time.end time.start) 60) 0.5)))
      (sql/modify db args.tkt "tickets" ["time"] {"time" (+ time.cur time.diff)})
      (sql/modify db rnum "records" ["time"] {"time" time.diff})
      (db:close)
      (print (.. "T" args.tkt " {prev: " time.cur ", cur: " (+ time.cur time.diff) ", log: " tnum "}")))))

;;(tkts/list "Open")
(fn tkts/list [status]
  "List tickets by status"
  (let
      [db (sql.open conf.tkt_loc)]
    (print (.. status " Tickets:"))
    (each [row (db:nrows (.. "SELECT * from tickets WHERE status = '" status "';"))]
      (print (.. (.. "T" row.id ") ") row.title)))
    (db:close)))

;;(tkts/edit/tkt 1)
(fn tkts/edit/tkt [tnum]
  "Serialize ticket data into tmp file, load in editor, serialize data from tmp file back to ticket db"
  (let
      [db (sql.open conf.tkt_loc)]
    (each [row (db:nrows (.. "SELECT * from tickets WHERE id = '" tnum "';"))]
      (let
          [tkt {"status" (. row "status")
                "title" (. row "title")
                "desc" (if (= (. row "desc") nil) "" (. row "desc"))
                "client" (if (= (. row "client") nil) "" (. row "client"))
                "project" (if (= (. row "project") nil) "" (. row "project"))
                "owner" (. row "owner")
                "time" (. row "time")}
           tmpf (os.tmpname)]
        (var diff {})
        ;;Serialize the tkt to a temp file
        (with-open [f (io.open tmpf "w")]
          (f:write (inspect tkt)))
        ;;Open the tmp file for editting
        (os.execute (.. conf.editor " " tmpf))
        ;;Reserialize the tmpf to the diff var
        (with-open [f (io.open tmpf "r")]
          (set diff (lume.deserialize (f:read "*a"))))
        ;;Update all ticket values in one pass.
        ;;TODO: Detect which values have been changed, and only queue an update query for those values
        (sql/modify db tnum "tickets" ["client" "desc" "owner" "project" "status" "time" "title"] diff)))
    (db:close)))

;;(log/edit 1 123)
(fn tkts/edit/log [tnum log]
  "Serialize record data into tmp file, load in editor, serialize data from tmp file back to record log"
  (let
      [db (sql.open conf.tkt_loc)
       lnum (tonumber log)
       recs (sql/select db "records" ["id" "log" "user" "time"] "=" ["tkt_num"] [tnum])
       log {"log" (. recs lnum "log") "user" (. recs lnum "user") "time" (. recs lnum "time")}
       tmpf (os.tmpname)]
    (var diff {})
    (with-open [f (io.open tmpf "w")]
      (f:write (inspect log)))
    (os.execute (.. conf.editor " " tmpf))
    (with-open [f (io.open tmpf "r")]
      (set diff (lume.deserialize (f:read "*a"))))
    (sql/modify db (. recs lnum "id") "records" ["log" "user" "time"] diff)
    (each [row (db:nrows (.. "SELECT sum(time) from records WHERE tkt_num = " tnum ";"))]
      (sql/modify db tnum "tickets" ["time"] {"time" (. row "sum(time)")}))))

(fn tkts/edit [args]
  (util/err "tkt" args "A tkt number is required to edit a tkt log.")
  (if (= args.log nil)
      (tkts/edit/tkt args.tkt)
      (tkts/edit/log args.tkt args.log)))

;;===== status =====
;;(status/log 1 10)
(fn status/log [tkt limit]
  (each [k record (pairs (rec/info tkt limit))]
    (let
        [log (. record "log")
         user (. record "user")
         time (. record "time")]
      (print (..  k ") {" user ", " time "}\n")
             log))))

;;(status/tkt {"tkt" 1 "verbose" true "records" 5})
(fn status/tkt [args]
  "Pretty print ticket information"
  (util/err "tkt" args "A tkt number is required to view info about a tkt.")
  (let
      [tkt (tkts/info args.tkt)]
    (print (.. (.. "Issue: " tkt.title "\n")
               (.. "Status: " tkt.status " | Owner: " tkt.owner "\n")
               (.. "Client: " tkt.client " | Proj: " tkt.project "\n")
               (.. "Desc: \n" tkt.desc "\n")
               (.. "Time: " tkt.time "min ($" tkt.rate ")\n")))
    (if (and (= args.verbose nil) (= args.records nil))
        (do
          (print "Log: " tkt.log)
          (status/log args.tkt 1))
        (~= args.records nil)
        (do
          (status/log args.tkt args.records))
        (= args.verbose true)
        (do
          (status/log args.tkt)))))

;;(status/comp {"name" "LambdaCreate"})
(fn status/comp [args]
  "Pretty print company information"
  (util/err "name" args "A company name is required to view info about a company.")
  (let
      [comp (comp/info args.name)]
    (if (= comp nil)
        (do
          (io.stderr:write "No company matching " args.name " found.\n")
          (os.exit))
        (do
          (print (.. (.. "Name: " comp.name "\n")
                     (.. "Abbrv: " comp.abbrv "\n")
                     (.. "Address: " comp.address "\n")
                     (.. "Comp ID: " comp.id "\n")
                     (.. "Create On: " comp.created_on)))))))

;;(status/clie {"name" "Will Sinatra" "company" "LambdaCreate"})
(fn status/clie [args]
  "Pretty print client information"
  (util/err "name" args "A client name is required to view information on a client.")
  (util/err "company" args "The company associated with the client is required to view information on a client.")
  (let
      [client (clie/info args.name args.company)]
    (if (= client nil)
        (do
          (io.stderr:write "No client found matching " args.name " in " args.company ".\n")
          (os.exit))
        (do
          (print (.. (.. "Name: " client.name "\n")
                     (.. "Company: " client.company "\n")
                     (.. "Role: " client.role "\n")
                     (.. "Phone: " client.phone "\n")
                     (.. "Email: " client.email "\n")
                     (.. "Client ID: " client.id "\n")
                     (.. "Created On: " client.created_on)))))))

;;(status/report {"filter" "all"})
(fn status/report [args]
  (let
      [db (sql.open conf.tkt_loc)
       count (tkts/count db)]
    (print (.. "Open: " count.open " | Closed: " count.closed))
    (if (or (= args.filter nil) (= (string.upper args.filter) "OPEN"))
        (tkts/list "Open")
        (= (string.upper args.filter) "CLOSED")
        (tkts/list "Closed")
        (= (string.upper args.filter) "ALL")
        (do
          (tkts/list "Open")
          (tkts/list "Closed")))))

;;(status/hours {"tkt" 1 "filter" "daily"})
;;filters: daily, weekly, monthly
;;((fn status/hours [args]
;;  "Generate hour breakdown report for a tkt"
;;  (util/err "tkt" args "A tkt is required to generate a report.")
;;  (util/err "filter" args "A filter is required, try daily, weekly, or monthly.")
;;  (let
;;      [db (sql.open conf.tkt_loc)
;;       report {"total_time" 0
;;               "daily_average" 0
;;               "weekly_average" 0
;;               "monthly_average" 0
;;               "daily" {}     ;; day_# = {"total" x}
;;               "weekly" {}    ;; week_# = {"total" x "average" x "day_1" x "day_7" x}
;;               "monthly" {}}]  ;; month_# = {"total" x "average" x "week_1" x "week_4" x}
;;       (each [row (db:nrows (.. "SELECT time,created_on from records WHERE tkt_num = '" args.tkt "';"))])))

;;===== mail =====
;;(tkts/mail {"tkt" 1})
(fn tkts/mail [args]
  (let
      [db (sql.open conf.tkt_loc)
       tkt (sql/select db "tickets" ["title" "client"] "=" ["id"] [args.tkt])
       mail (sql/select db "client" ["email"] "=" ["name"] [(. tkt 1 "client")])]
    (os.execute (.. conf.mail " '" (. tkt 1 "title") "' " (. mail 1 "email")))))

;;;;===== invoice =====
;;(tkts/invoice {"tkt" 1})
(fn tkts/invoice [args]
  "Generate an invoice from data in a tkt"
  (let
      [groff (with-open [f (io.open conf.invoice_template "r")]
               (f:read "*a"))
       db (sql.open conf.tkt_loc)
       timestamp (os.time)
       issued (os.date "%x" timestamp)
       due (os.date "%x" (+ timestamp (* (* (* 30 24) 60) 60))) ;;due 30 days from issue
       tkt (tkts/info (. args "tkt"))
       user (clie/info conf.user conf.company)     ;;expects the user to have a record detailing themselves
       user_company (comp/info (. user "company")) ;;expects the user to have a record detailing themselves
       client (clie/info (. tkt "client") "%")     ;;expects the tkt to have client & company records
       company (comp/info (. client "company"))
       address (util/split (. company "address") ",")]
    (print (util/template {1 groff
                           :full_name (. user "name")
                           :address (. user_company "address")
                           :cell_number (. user "phone")
                           :company (. client "company")
                           :invoice_number (.. (. args "tkt") (string.gsub issued "/" ""))
                           :company_addr (.. (. address 1) "," (. address 2))
                           :company_zip (string.gsub (. address 3) "^ " "")
                           :issued_date issued
                           :due_date due
                           :client_email (. client "email")
                           :client_cellphone (. client "phone")
                           :tkt_title (. tkt "title")
                           :hours (string.format "%.3f" (/ tkt.time 60))
                           :hourly conf.rate
                           :hours_cost (math.ceil (* (/ tkt.time 60) conf.rate))}))))

;;=====[doc]=====
;;(tkts/doc {"tkt" 1})
(fn tkts/doc [args]
  "Generate a markdown document of a tkt's history"
  (let
      [tkt (tkts/info args.tkt)]
    (print (.. "# T" args.tkt " " tkt.title))
    (print tkt.desc)
    (each [k record (pairs (rec/info args.tkt nil))]
      (let
          [date (. record "created_on")
           log (. record "log")]
        (print (..  "## " date "\n" log "\n"))))))


;;===== main =====
;;(main/usage {"verbose" true})
(fn main/usage [args]
  (let
      [author (.. ";;===== tkts " ver.tkts " =====
Author: Will Sinatra <wpsinatra@gmail.com>
License: GPLv3")
       overview "usage: tkts <command> [subcommand] [<flag> <arg>]
help [-s|--segment tkt, company, client, experimental, all]

tkts commands:
  nil      |  Ticket record module
  company  |  Company record module
  client   |  Client record module
  invoice  |  Invoice generation"
       tkt "
;;=====[tkt]=====
tkts open -i|--issue [-d|--description]                   |   Create a new ticket, requires an issue title
tkts close -t|--tkt                                       |   Close an existing ticket, requires a tkt number
tkts reopen -t|--tkt                                      |   Reopen a previously closed ticket, requires a tkt number
tkts info -t|--tkt [-v|--verbose] [-r|--records]          |   Show details about a tkt, verbose shows the entire record log, records limits it to the last X records.
tkts edit -t|--tkt [-l|--log]                             |   Edit an existing tkt's values, or if passed a log number, modify the record specified
tkts log -t|--tkt [-l|--log]                              |   Add a log record to a ticket, requires a tkt number, if passed --log, append without opening the editor
tkts time -t|--tkt                                        |   Opens an editor and records time spent writing notes, then appends record and time to tkt
tkts status -f|--filter open,closed,all [-v|--verbose]    |   Generate a top level view of tkts by status."  
       company "
;;=====[company]=====
tkts company help                                                  |   Get usage info for the company subcommand
tkts company add -n|--name -abv|--abbreviation [-addr|--address]   |   Add a company record, requires a name and an abbreviation
tkts company edit -n|--name                                        |   Edit an existing company, requires the name of the company to edit
tkts company list                                                  |   List all company records
tkts company info -n|--name                                        |   Show details about a company, requires the name of the company"
       client "
;;=====[client]=====
tkts client help                          |   Get usage info for the client subcommand
tkts client add -n|--name -c|--company    |   Add a client record, requires the client's name and their company
tkts client edit -n|--name -c|--company   |   Edit an existing client record, requires the client's name and their company
tkts client list                          |   List all clients
tkts client info -n|--name -c|--company   |   Show details about a client, requires the client's name and their company"
       invoice "
;;=====[invoice]=====
tkts invoice -t|--tkt   |   Generate an invoice in groff format using data from the tkt provided"
       experimental "
;;=====[experimental]=====
tkts mail -t|--tkt      |   Send an email update to a client specified on a tkt (broken)
tkts doc -t|--tkt       |   Generate a markdown document of a tkt's history"
       brief "Change a tkt's status:
open -i|--issue [-d|--description]
close -t|--tkt
reopen -t|--tkt

Edit a tkt's records:
edit -t|--tkt [-l|--log]
log -t|--tkt [-l|--log]
time [-t|--tkt]

Determine a tkt's status:
info -t|--tkt [-v|--verbose]
status [-f|--filter] [-v|--verbose]"]
    (match args.segment
      nil (print (.. author "\n" overview "\n" tkt))
      :all (print (.. author "\n" overview "\n" tkt "\n" company "\n" client "\n" invoice "\n" experimental))
      :brief (print (.. author "\n" brief))
      :tkt (print (.. author "\n" overview "\n" tkt))
      :company (print (.. author "\n" overview "\n" company))
      :client (print (.. author "\n" overview "\n" client))
      :invoice (print (.. author "\n" overview "\n" invoice))
      :experimental (print (.. author "\n" overview "\n" experimental))))
  (os.exit))

;;Parse command, subcommand, and flags
(fn main [arg]
  (util/conf)
  (match arg
    [nil] (status/report {})
    [:help & ?args] (main/usage (util/farse ["--segment" "-s"] ?args))
    [:open & ?args] (tkts/create (util/farse ["--issue" "--description" "-i" "-d"] ?args))
    [:close & ?args] (tkts/close (util/farse ["--tkt" "-t"] ?args))
    [:reopen & ?args] (tkts/reopen (util/farse ["--tkt" "-t"] ?args))
    [:info & ?args] (status/tkt (util/farse ["--tkt" "--records" "--verbose" "-t" "-r" "-v"] ?args))
    [:log & ?args] (tkts/comment (util/farse ["--tkt" "--log" "-t" "-l"] ?args))
    [:edit & ?args] (tkts/edit (util/farse ["--tkt" "--log" "-t" "-l"] ?args))
    [:time & ?args] (tkts/time (util/farse ["--tkt" "-t"] ?args))
    [:mail & ?args] (tkts/mail (util/farse ["--tkt" "-t"] ?args))
    [:status & ?args] (status/report (util/farse ["--filter" "-f"] ?args))
    [:invoice & ?args] (tkts/invoice (util/farse ["--tkt" "-t"] ?args))
    [:doc & ?args] (tkts/doc (util/farse ["--tkt" "-t"] ?args))
    [:company & ?args] (match ?args
                         [nil] (main/usage {"segment" "company"})
                         [:help] (main/usage {"segment" "client"})
                         [:add & ?flags] (comp/add (util/farse ["--name" "--abbreviation" "--address" "-n" "-abv" "-addr"] ?flags))
                         [:edit & ?flags] (comp/edit (util/farse ["--name" "-n"] ?flags))
                         [:list] (comp/list)
                         [:info & ?flags] (status/comp (util/farse ["--name" "-n"] ?flags)))
    [:client & ?args] (match ?args
                        [nil] (main/usage {"segment" "client"})
                        [:help] (main/usage {"segment" "client"})
                        [:add & ?flags] (clie/add (util/farse ["--name" "--company" "-n" "-c"] ?flags))
                        [:edit & ?flags] (clie/edit (util/farse ["--name" "--company" "-n" "-c"] ?flags))
                        [:list] (clie/list)
                        [:info] (status/clie (util/farse ["--name" "--company" "-n" "-c"] ?args)))
    _ (main/usage {})))

(main arg)
