# tkts

## What?
A simple cli driven ticketing system.

## Why?
This project initially started simply because. I was curious to see if I could write a functional ticketing system, what kind of programming was required to get the bare minimum functionality. As the project progressed I became pretty enamored with my silly toy ticketing system and added features, stuck it inside of an IRC bot, and got lots of feedback from friends using the program to manage their Alpine packages and tilde projects.

During that time I spent a whole year tracking my internal projects, Alpine packages, and homelab issues using tkts. I honed in on missing features and rough edges thanks to my own testing and the feedback of friends. Thanks to that tkts has grown today to be a simple but functional ticketing solution for low count multi-user teams or single agents.

My intent with tkts is to provide a tool that can be used for small sysadmin/support teams to track and maintain issues, or for contractors to handle coordination for contractual labor performed for a client.

## Contributions & Suggestions:
If you use tkts, find it interesting, and especially if you have ideas of suggestions that would benefit and extend tkts, please feel free to reach out either by email or create an issue on the git repo herein!

## Overview:
Disclaimer: This image is out of date and shows tkts at around v1.2
I'll updated it shortly.

![tkts Useage Overview](.img/overview.gif)

### Roadmap:
- [x] Release on Alpine (v1.0.1 in Testing)
- [x] Integrate with $EDITOR of choice. (v1.2)
- [x] Migrate to sqlite3 based backend. (v2.0)
- [ ] Extend Documentation (to include multi user and IRC use).
- [ ] Extend Client/Company functionality.
- [ ] Extend log manipulation functions.
- [ ] Simple webfrontend (potentially lapis based)

## Building:
You can build tkts by calling the following commands. Dependencies are sourced via luarocks. Luarocks, lua5.3, and lua5.3-dev files are required to compile the binary. only Luarocks and Lua5.3 are required to produce the lua script.

### Static Fennel Binary
* make compile-bin
* sudo make install-bin

### Executable Lua Script
* make compile-lua
* make install-lua

## Usage:
```
;;===== tkts 2.0 =====
Author: Will Sinatra <wpsinatra@gmail.com>
License: GPLv3
;;===== Usage =====
[-o issue ?desc] [-c tnum] [-r tnum] [-al tnum ?log] [-t tnum ?time] [-s ?status] [-e tnum]
company [-a name] [-e name] [-li] [-i name]
client [-a name company] [-e name company] [-li] [-i name company] 
help [-h] [-v]

Tkts Namespace:
tkts                                | Show currently opened tickets
tkts -o "new issue"                 | Open a ticket
tkts -o "new issue" "desc"          | Open a ticket with a description
tkts -c T#                          | Close a ticket
tkts -r T#                          | Reopen a ticket
tkts -al T#                         | Add a log to the ticket via editor
tkts -al T# "log info to ticket"    | Add a log to the ticket via string
tkts -t T#                          | Add logging to ticket via editor, while recording time spent
tkts -t T# #                        | Add time to the ticket
tkts -i T# ?-v                      | Return ticket information, -v for full log
tkts -e T#                          | Editan existing ticket's data
tkts -s [open,closed,all] ?-v       | Report tickets by status, -v for info

Company Namespace:
tkts company -a "name" "short"      | Add a new company
tkts company -e "name"              | Edit an existing company
tkts company -li                    | List all companies
tkts company -i "name"              | Show company details

Client Namespace:
tkts client -a "name" "company"     | Add a client, associated with company
tkts client -e "name" "company"     | Edit an existing client
tkts client -li                     | List all clients
tkts client -i "name" "company"     | Show client details
```

## Configuring tkts
After installing, run any tkts command, the easiest being to just run the tkts command itself to ensure that it works. If installed correctly it should show an empty ticket report.

Running this command produces a coniguration file and database at ~/.config/tkts named tkts.conf & tkts.db respectively.

The contents of tkts.conf define user specific settings, such as your EDITOR and the location of the tkts.db file.
```
{["user"]="durrendal",["rate"]="00.00",["tkt_loc"]="/home/durrendal/.config/tkts/tkts.db",["editor"]="mg"}
```

### Configuring email handling (WIP Feature)
There's probably more that can be done here, but basic handling for now looks like this with a handful of clients.

The core functionality for now just passes the ticket title as the subject, and pulls client's email if one is defined, and you can tweak inside of your client afterwards if it's supported. I use neomutt personally, so that's how the workflow is designed currently.

```
["mail"]="mutt -s"
["mail"]="mailx -s"
```

So if you have a tkt like this:
```
Issue: [H] - Aarch64 LXC Node
Status: Open | Owner: durrendal
Client: Internal | Proj: Homelab
Desc: 
Image the RockPro board and add it to the LXD cluster
Time: 10min ($8)

Log:    1
```
and a client configuration like this:
```
Name: durrendal
Company: LambdaCreate
Role: Developer
Phone: (123) 456-7890
Email: durrendal@lambdacreate.com
Client ID: 1
Created On: 2022-01-10 22:05:00
```
the mail function would essentially pass the following
```
mutt -s "[H] - Aarch64 LXC Node" durrendal@lambdacreate.com
```

## Invoice Generation
You need groff installed, but tkts will happily pull info from a ticket and output a groff invoice template. The one I designed is simple and you might want to modify it yourself it's saved in ~/.config/tkts/invoice.tf.

```
tkts invoice -t 2 | tbl | groff -me -Tpdf -ms > invoice.pdf
```

TODO: Document the fields needed in the invoice.tf file for tkts to populate it.
TODO: Document how the invoice groff works

## License:
GNU General Public License v3.0

## Icon:
Ticket by Eucalyp from the Noun Project
